#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Filter for detecting suspicious user activity. After 3 successive
    failed login attempts within 20 any further request within a 5 minutes
    will be logged.

    Solution idea: each ip address has 3 states: insuspicious, candidate, and blocked.
    We can code this in a lazy evaluation style. Every time a new request arrives
    we check if the the ip address falls into one of those states by keeping
    a dictionary.
"""

from __future__ import print_function

import datetime

TIMESTAMP_FORMAT = '%d/%b/%Y:%H:%M:%S'
MON = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6,
       'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12}


def strptime(val):
    """
    Custom strptime function, since builtin datetime.datetime.strptime is
    very flexible but very slow

    """

    return datetime.datetime(int(val[7:11]),  # year
                             MON[val[3:6]],  # month
                             int(val[:2]),  # day
                             int(val[12:14]),  # hour
                             int(val[15:17]),  # minute
                             int(val[18:20]))  # second


class BlockedCandidate(object):
    """
        Convience class for keeping track of blocked IP candidates
        Only attribute here is the start-time.

        Need to keep try of start of time-window and how
        many failed attempts have been registered.
    """
    def __init__(self, start_time, fail_count=1):
        self.start_time = start_time
        self.fail_count = fail_count

    def __repr__(self):
        return str(self.start_time)


class BlockedIP(object):
    """
        Convience class for keeping track of blocked IPs
        Only attribute here is the start-time.
    """
    def __init__(self, start_time):
        self.start_time = start_time

    def __repr__(self):
        return str(self.start_time)


def main1(infile, short_interval=20, long_interval=300, fail_status='401'):
    """
    Task: Log blocked login attempts:
    3 consecutive login attempts within 20 seconds
    block all request from same host for next 5 minutes window.

    Log all blocked attempts.

    Runtime analysis:
        Streaming O(n)
    Memory:
        memory will grow when there are a lot of different blocked IPS
        or blocked candidates. In practice this might become a problem
        after very long runtime. Thus once a day or so the candidate and
        blocked dictionaries should be pruned.

    Detailed comments in source code.
    """
    last_timestr = ''
    blocked_candidates = {}
    blocked_ips = {}
    blocked_requests = []
    for line in infile:
        spline = line.split()
        timestr = spline[3][1:]
        ip = spline[0]
        try:
            # count from backwards because request can be empty ""
            status = spline[-2]
        except:
            raise UserWarning("Hickup in line {i}: {line}".format(**locals()))
        if timestr != last_timestr:
            timestamp = strptime(timestr)
            last_timestr = timestr

        if ip in blocked_ips:
            if (timestamp -
                    blocked_ips[ip].start_time).total_seconds() \
                    <= long_interval:
                # if in long_interval
                # log attempt regardless of status
                blocked_requests.append(line)
            else:
                if status == fail_status:  # if failure
                    blocked_candidates[ip] = BlockedCandidate(
                        timestamp)  # create new blockedcandidate
                del blocked_ips[ip]  # remove from blocked ips
        elif ip in blocked_candidates:
            if (timestamp -
                    blocked_candidates[ip].start_time).total_seconds() <= \
                    short_interval:
                # if in short interval
                if status == fail_status:  # if failure
                    # increase failure count
                    blocked_candidates[ip].fail_count += 1
                    # if failure count == 3
                    if blocked_candidates[ip].fail_count == 3:
                        blocked_ips[ip] = BlockedIP(timestamp)
                        del blocked_candidates[ip]
                        # move ip from blocked_candidate to blocked_ips
                else:
                    del blocked_candidates[ip]  # remove from candidate
            else:
                if status == fail_status:  # if failure
                    blocked_candidates[ip].start_time = timestamp
                    # reset failure count to 1 and update timestamp of
                    # candidate
                    blocked_candidates[ip].fail_count = 1
                else:
                    # remove from blocked_candidates
                    del blocked_candidates[ip]
        else:
            if status == fail_status:  # if failure
                blocked_candidates[ip] = BlockedCandidate(
                    timestamp)  # create new candidate

    return blocked_requests

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        raise UserWarning("Input log.txt expected as argument")
    OUTPATH = sys.argv[2] if len(sys.argv) >= 3 else '.'

    with open(sys.argv[1]) as infile:
        print("\nFEATURE 4: SUSPICIOUS LOGIN ATTEMPTS")
        BLOCKEDS = main1(infile)

    with open(OUTPATH + '/blocked.txt', 'w') as outfile:
        for blocked in BLOCKEDS:
            outfile.write(blocked)

        print("\n" + str(len(BLOCKEDS)) + " requests blocked.")
