#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Demo implementation for extracting the IP addresses/host
    that generate the most visits based on a given server log.
"""

from __future__ import print_function

def main1(infile):
    """
    Brute Force Solution
    Read all lines, count ip occurence using dictionary/hash
    Sort dictionary by `values` and emite top 10 values

    Runtime analysis
    Read values: time O(n), memory O(n)
    Sort values time O(n log(n)), memory O(n)

    """
    # READ
    visitor_count = {}
    for i, line in enumerate(infile):
        ip_addr = line.split()[0]
        visitor_count[ip_addr] = visitor_count.get(ip_addr, 0) + 1
    # SORT
    items = sorted(((value, key)
                    for (key, value)
                    in visitor_count.items()),
                   reverse=True)
    # OUTPUT
    visitors = []
    for i, (count, visitor) in enumerate(items):
        if i == 10:
            break
        print(" {i} {visitor} {count}".format(**locals()))
        visitors.append(visitor + ',' + str(count))

    # solution 1 : runtime about 12 seconds
    return visitors

if __name__ == '__main__':
    import sys

    if len(sys.argv) < 2:
        raise UserWarning("Input log.txt expected as argument")
    OUTPATH = sys.argv[2] if len(sys.argv) >= 3 else '.'

    with open(sys.argv[1]) as infile:
        print("\nFEATURE 1: TOP 10 VISITORS")
        VISITORS = main1(infile)

    with open(OUTPATH + '/hosts.txt', 'w') as outfile:
        for visitor in VISITORS:
            outfile.write(visitor + '\n')
