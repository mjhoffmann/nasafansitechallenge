#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Slight variant of feature 3: this time we look for non-overlapping
    time-windows of heavy load. This could be useful in actually detecting
    separate real-world events that generate traffic.
"""

from __future__ import print_function

import collections
import datetime
import heapq

TIMESTAMP_FORMAT = '%d/%b/%Y:%H:%M:%S'

MON = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6,
       'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12}


def strptime(val):
    """
    Custom strptime function, since builtin datetime.datetime.strptime is
    very flexible but very slow
    """

    return datetime.datetime(int(val[7:11]),  # year
                             MON[val[3:6]],  # month
                             int(val[:2]),  # day
                             int(val[12:14]),  # hour
                             int(val[15:17]),  # minute
                             int(val[18:20]))  # second


class BusyHour(object):
    """
    Convenience class to make keeping track (and sorting)
    of busy time-slots easy.
    """
    def __init__(self, entries):
        self.length = len(entries)
        self.start_time = entries[0]

    def __repr__(self):
        return self.start_time.strftime(TIMESTAMP_FORMAT) \
            + ',' \
            + str(self.length)

    def __cmp__(self, other):
        return cmp((self.length), (other.length))


def main1(infile, topn=10, td_seconds=3600):
    """
        Task: determine the busiest 60 minute slots

        Use a queue for keeping track of load level of current timestamp,
        Use a heap to main top 10 busiest hours while scanning.
    """
    curr_window = collections.deque()
    topn_hours = []
    last_timestr = ''

    for line in infile:
        timestr = line.split()[3][1:]
        if timestr != last_timestr:
            timestamp = strptime(timestr)
            last_timestr = timestr
        curr_window.append(timestamp)

        while curr_window \
                and (timestamp - curr_window[0]).total_seconds() > td_seconds:
            curr_window.popleft()

        if topn_hours:
            if len(curr_window) > topn_hours[0].length:
                if any((curr_window[0] -
                        x.start_time).total_seconds() <= td_seconds
                       for x in topn_hours):
                    for j, topn_hour in enumerate(topn_hours):
                        if (curr_window[0] -
                                topn_hour.start_time).total_seconds() <= \
                                td_seconds \
                                and len(curr_window) > topn_hour.length:
                            topn_hours[j] = topn_hours[-1]
                            topn_hours.pop()
                            topn_hours.append(BusyHour(curr_window))
                            heapq.heapify(topn_hours)
                            break
                else:
                    if len(topn_hours) > topn:
                        heapq.heappushpop(topn_hours, BusyHour(curr_window))
                    else:
                        heapq.heappush(topn_hours, BusyHour(curr_window))
        else:
            topn_hours = [BusyHour(curr_window)]

    if last_timestr != '':
        # assuming all timestamps in one test are recorded in same timezone
        offset = line.split()[4][:-1]

    return [(x.start_time, x.length) for x in reversed(topn_hours)], offset

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        raise UserWarning("Input log.txt expected as argument")
    OUTPATH = sys.argv[2] if len(sys.argv) >= 3 else '.'

    with open(sys.argv[1]) as infile:
        HOURS, OFFSET = main1(infile)

    with open(OUTPATH + '/hours_separate.txt', 'w') as outfile:
        for hour, count in sorted(HOURS, key=lambda x: -x[1]):
            start_time = hour.strftime(TIMESTAMP_FORMAT) + ' ' + OFFSET
            outfile.write(start_time + ',' + str(count) + '\n')
            print(start_time + ',' + str(count))
