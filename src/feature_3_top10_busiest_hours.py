#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Demo implementation of finding the top(N=10) busiest
    60 min time-slots from a server log-file. Note that a
    time-slot can start at any second and not just when
    a request occurs.
"""

from __future__ import print_function

import collections
import datetime
import heapq

TIMESTAMP_FORMAT = '%d/%b/%Y:%H:%M:%S'
MON = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6,
       'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12}


def strptime(val):
    """
    Custom strptime function, since builtin datetime.datetime.strptime is
    very flexible but very slow
    """

    return datetime.datetime(int(val[7:11]),  # year
                             MON[val[3:6]],  # month
                             int(val[:2]),  # day
                             int(val[12:14]),  # hour
                             int(val[15:17]),  # minute
                             int(val[18:20]))  # second


class BusyHour(object):
    """
    Convenience class to make keeping track (and sorting)
    of busy time-slots easy.
    """
    def __init__(self, entries, start_time):
        self.length = len(entries)
        self.start_time = start_time

    def __repr__(self):
        return datetime.datetime.fromtimestamp(
            self.start_time).strftime(TIMESTAMP_FORMAT) \
            + ',' \
            + str(self.length)

    def __cmp__(self, other):
        return self.length - other.length


def main1(infile, topn=10, td_seconds=3600):
    """
        Task: determine the busiest 60 minute slots

        Use a queue for keeping track of load level of current timestamp,
        Use a heap to maintain top 10 busiest hours while scanning.
    """

    curr_window = collections.deque()
    # use a double-ended queue (deque) to be able to
    # quickly push and pop at beginning and end.

    # maintain the topX sliding window while
    # while traversing as heap

    start_time = 0
    line = infile.readline()
    # if log file is empty, return empty list
    if not line:
        return [], ''

    # prep loop with first line
    timestr = line.split()[3][1:]
    offset = line.split()[4][:-1]
    curr_time = int(strptime(timestr).strftime('%s'))
    start_time = end_time = curr_time
    curr_window = collections.deque([curr_time])
    topn_hours = [BusyHour([start_time], start_time)]

    while True:
        while start_time + td_seconds > end_time:
            line = infile.readline()
            if line != '':
                timestr = line.split()[3][1:]
                end_time = int(strptime(timestr).strftime('%s'))
                curr_window.append(end_time)
            else:
                break
        else:
            curr_window.append(end_time)

        while start_time <= curr_window[0]:
            if len(curr_window) > topn_hours[0].length:
                if len(topn_hours) < topn:
                    heapq.heappush(topn_hours, BusyHour(
                        curr_window, start_time))
                else:
                    heapq.heappushpop(topn_hours, BusyHour(
                        curr_window, start_time))
            start_time += 1

        while curr_window and start_time >= curr_window[0]:
            curr_window.popleft()

        if len(curr_window) == 0:
            break
    return [(x.start_time, x.length)
            for x in reversed(sorted(topn_hours))], offset

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        raise UserWarning("Input log.txt expected as argument")
    OUTPATH = sys.argv[2] if len(sys.argv) >= 3 else '.'

    with open(sys.argv[1]) as infile:
        print("\nFEATURE 3: TOP 10 BUSIEST 60 MINUTE WINDOWS")
        HOURS, OFFSET = main1(infile)

    with open(OUTPATH + '/hours.txt', 'w') as outfile:
        for hour, count in sorted(HOURS):
            start_time = datetime.datetime.fromtimestamp(
                hour).strftime(TIMESTAMP_FORMAT) + ' ' + OFFSET
            outfile.write(start_time + ',' + str(count) + '\n')
            print(start_time + ',' + str(count))
