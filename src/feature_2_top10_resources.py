#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Implement the task to identify server resources (assets) that generate the most traffic.


"""

from __future__ import print_function


def main1(infile):
    """
    Brute Force solution
    Read in all lines, store transferred data volume
    for each pathname in dict and add up
    Then sort dictionary by values and emit top 10 results


    Runtime analysis
    Read in values: time O(n), memory O(n)
    Sort by values: time O(n*log(n)), memory O(n) [python/timsort]
    """
    resource_count = {}
    for i, line in enumerate(infile):
        spline = line.split()
        spline2 = line.split('"')
        resource = spline2[1]
        if '-' not in spline[-1]:
            load = int(spline[-1])
            resource_count[resource] = resource_count.get(resource, 0) + load

    items = sorted(((value, key)
                    for (key, value)
                    in resource_count.items()),
                   reverse=True)

    resources = []
    print("TOP 10 RESOURCES")
    for i, (count, resource) in enumerate(items):
        if i == 10:
            break
        print(" {i} {resource} {count}".format(**locals()))
        resources.append(resource.split()[1])

    # solution 1 : runtime about 12 seconds
    return resources

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        raise UserWarning("Input log.txt expected as argument")
    OUTPATH = sys.argv[2] if len(sys.argv) >= 3 else '.'
    with open(sys.argv[1]) as infile:
        print("\nFEATURE 2: TOP 10 MOST TRAFFIC HEAVY ASSETS")
        RESOURCES = main1(infile)

    with open(OUTPATH + '/resources.txt', 'w') as outfile:
        for resource in RESOURCES:
            outfile.write(resource + '\n')
