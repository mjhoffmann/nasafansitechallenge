# NASA Fan Site Coding Challenge

## General

Solutions are implemented in Python and tested again Python v2.7.
I also tried running against Python 3, but some encoding issues
in input file made it difficult to write code compatible with
both versions.


## Dependencies

Solutions only require Python 2.7 built-in libraries such as
- datetime
    For processing and formatting timestamps
- heapq
    To keep track of top(N=10) items while iterating through line
- collections
    Use deque data structure to maintain 60 min sliding window

## Comments on features

### Feature 1

Straightforward to read-in, sort, and report
For significantly larger files this could run into memory limitations
but given the size of input the choice was made to process everything in memory

### Feature 2

Similar as 1

### Feature 3

- Used doubled-ended queue (deque) to maintain sliding window of 60 minutes.
- Used heap to efficiently maintain top(N=10) windows
- Output doesn't seem too useful to me since it essentially mirrors only one large events
- Instead feature 3b was implemented to produce the top(N=10) _non-overlapping_ time windows
  This could be useful to analyse separate traffic spikes

### Feature 4

- Used 2 dictionaries to keep track of candidates for blocked address (20 second window)
  and blocked address (5 minute window)
- Seems efficient because of fast lookup in dictionaries (hashing) and only requires processing
  when new requests arrive
- For many simultaneous blocked and candidate address this could require increasingly more memory
- As a remedy the blocked and candidate IPs should be cleaned (e.g. once per day)



## Log Output (./run.sh ) &> OUTPUT.txt
+ ./src/feature_1_top10_visitors.py log_input/log.txt log_output

FEATURE 1: TOP 10 VISITORS
 0 piweba3y.prodigy.com 22309
 1 piweba4y.prodigy.com 14903
 2 piweba1y.prodigy.com 12876
 3 siltb10.orl.mmc.com 10578
 4 alyssa.prodigy.com 10184
 5 edams.ksc.nasa.gov 9095
 6 piweba2y.prodigy.com 7961
 7 163.206.89.4 6520
 8 www-d3.proxy.aol.com 6299
 9 vagrant.vf.mmc.com 6096

real	0m14.508s
user	0m13.150s
sys	0m0.736s
+ ./src/feature_2_top10_resources.py log_input/log.txt log_output

FEATURE 2: TOP 10 MOST TRAFFIC HEAVY ASSETS
TOP 10 RESOURCES
 0 GET /shuttle/missions/sts-71/movies/sts-71-launch.mpg HTTP/1.0 4830802668
 1 GET / HTTP/1.0 4820688397
 2 GET /shuttle/missions/sts-71/movies/sts-71-tcdt-crew-walkout.mpg HTTP/1.0 4505833864
 3 GET /shuttle/missions/sts-53/movies/sts-53-launch.mpg HTTP/1.0 3160325280
 4 GET /shuttle/countdown/count70.gif HTTP/1.0 1959003012
 5 GET /shuttle/technology/sts-newsref/stsref-toc.html HTTP/1.0 1255980344
 6 GET /shuttle/countdown/video/livevideo2.gif HTTP/1.0 1169155568
 7 GET /shuttle/countdown/count.gif HTTP/1.0 1154002480
 8 GET /shuttle/missions/sts-71/movies/sts-71-hatch-hand-group.mpg 1135756810
 9 GET /shuttle/countdown/video/livevideo.gif HTTP/1.0 1128333928

real	0m10.737s
user	0m10.108s
sys	0m0.313s
+ ./src/feature_3_top10_busiest_hours.py log_input/log.txt log_output

FEATURE 3: TOP 10 BUSIEST 60 MINUTE WINDOWS
13/Jul/1995:08:59:34 -0400,36639
13/Jul/1995:08:59:35 -0400,36639
13/Jul/1995:08:59:36 -0400,36632
13/Jul/1995:08:59:37 -0400,36632
13/Jul/1995:08:59:38 -0400,36632
13/Jul/1995:08:59:39 -0400,36632
13/Jul/1995:08:59:40 -0400,36634
13/Jul/1995:08:59:41 -0400,36634
13/Jul/1995:08:59:42 -0400,36628
13/Jul/1995:08:59:43 -0400,36628

real	1m0.679s
user	0m57.624s
sys	0m0.876s
+ ./src/feature_4_top10_consecutive_login_failures.py log_input/log.txt log_output

FEATURE 4: SUSPICIOUS LOGIN ATTEMPTS

1758 requests blocked.

real	0m8.980s
user	0m8.598s
sys	0m0.253s
