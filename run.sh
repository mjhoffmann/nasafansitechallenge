#!/bin/bash -eux

time ./src/feature_1_top10_visitors.py log_input/log.txt log_output 2>&1
time ./src/feature_2_top10_resources.py  log_input/log.txt log_output 2>&1
time ./src/feature_3_top10_busiest_hours.py log_input/log.txt log_output 2>&1
time ./src/feature_4_top10_consecutive_login_failures.py log_input/log.txt log_output 2>&1
